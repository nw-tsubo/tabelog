# 食べログクローンの概要説明

- このレッスンで学べること
 - CRUD
 - フレッシュメッセージ
 - ページネーション
 - ファイルアップロード
 - ビューのエレメント
 - カスタムバリデーション
 - モデルのコールバックメソッド
 - 認証（ログイン）
 - アソシエーション（DBのリレーション）
 - Ajax
- 作成するアプリケーションの概要
- 環境の説明
 - PHP, CakePHP, MySQL, jQuery のバージョン
 - Vagrant 環境でやることの説明　← XAMPP でやりませんか？

----

# CakePHP のセットアップ

- CakePHP のプロジェクトファイル展開（zipファイルをダウンロードして手動インストール）
- app/tmp の権限の設定
- セキュリティの設定（salt, cipherSeed）

----

# データベースの設定

- テーブル構造の説明（shops, users, reviews）
- app/Config/database.php の設定
- テーブルの Create
- テストデータの Insert

----

# DebugKit の導入

- プラグインのインストール（zipファイルをダウンロードして手動インストール）
- DebugKit の説明

----

# CRUD

## レストラン一覧

## レストラン詳細

## レストラン登録

- フラッシュメッセージ
- バリデーション

## レストラン編集

## レストラン削除

----

# ページネーション

- Paginator コンポーネントを使って、ページネーションを実装

----

# ビューのエレメント

## ビューのエレメントを使ってナビゲーションメニューを実装

## フォームのエレメント化

----

# ファイルアップロード

- ショップ画像のアップロードと表示を実装
- Upload プラグインを使用する
- ALTER TABELE SQL文で既存のテーブルに項目を追加
- モデルのビヘイビアを使う
- アプリケーション固有の設定ファイルを作成して読み込む
- 写真の img タグを生成するヘルパーを作成

※ Upload プラグインは CakePHP ver.2 に対応したものを使うよう注意する
https://github.com/josegonzalez/cakephp-upload/tree/2.x
http://cakephp-upload.readthedocs.io/en/2.x/

----

# 認証と承認（ログインと権限）

- Auth コンポーネントを使ってログイン機能の実装する

## ユーザー登録

- カスタムバリデーションを使って、パスワードの再入力チェックを行う
- モデルのコールバックメソッドを使ってパスワードをハッシュ化して保存する

## Auth コンポーネント

- Auth コンポーネントを説明
- AppController で Auth コンポーネントを設定（コメント参照）

http://book.cakephp.org/2.0/ja/tutorials-and-examples/blog-auth-example/auth.html
http://book.cakephp.org/2.0/ja/core-libraries/components/authentication.html

## ログイン／ログアウト

- UserController で login, logout を実装

## アクセス許可／拒否

- ShopController, UserController の beforeFillter で 公開アクションを指定
- AppController の beforeFilter で ビューに カレントユーザーの情報をセット
- ナビゲーションバーにメニューを追加して、ログイン状況に応じて表示を変更
- レストラン一覧で未ログイン時に削除を表示しないよう修正

## ユーザー編集

----

# アソシエーション（DBのリレーション）

- レビューの登録・編集機能の追加レッスンを通してアソシエーションを実装

## レビュー登録・編集

- Review モデルを作成し、belongsTo を使う
- Review の登録と編集機能を実装
- レビューの編集画面にルーティングの設定を使ってみる

## レビュー一覧

- Shop, User モデルに、hasMany を追加
- レストラン画面にレビューの一覧を表示
- レストラン一覧にレビューの評価を表示（Shop データの取得で JOIN と集計関数を使用）

## レビュー削除

- レビュー編集画面にレビューの削除ボタンを追加
- レストラン一覧でレストランが削除されたら、関連するレビューも一緒に削除する

----

## セキュリティー

- ビューで h() 関数を使って、XSS 対策を行う
- Security コンポーネントを使って、CSRF と Mass Assignment 対策を行う

----

## コンソールアプリケーション

- HelloShell.php でコンソールアプリケーション版の Hello World を作成
- ShopsShell.php で Shops テーブルへの一括データ登録と一括削除機能を作成

----

# Ajax

## jQuery のセットアップ

## 無限スクロールを jQuery の ajax で実装

- ShopsController を ajax リクエストの時はレイアウトなしのビューを返すよう修正
- レストラン一覧画面の最下部にスクロールした時に、ショップデータを非同期で追加するよう、JavaScript ファイルを作成し、ビューを修正

---

# セキュリティー

- h()
- mass assignment
- CSRF

