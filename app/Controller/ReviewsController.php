<?php

class ReviewsController extends AppController {
    public $uses = ['Review', 'Shop'];

    public function edit($shop_id = null) {
        if (!$this->Shop->exists($shop_id)) {
            throw new NotFoundException('レストランが見つかりません');
        }
        $user_id = $this->Auth->user('id');

        if ($this->request->is(['post', 'put'])) {
            $message = 'レビューを更新しました。';
            if (empty($this->request->data['Review']['id'])) {
                $message = 'レビューを登録しました。';
                $this->Review->create();
            }

            $this->request->data['Review']['user_id'] = $user_id;

            if ($this->Review->save($this->request->data)) {
                $this->Flash->success($message);

                return $this->redirect([
                    'controller' => 'shops',
                    'action' => 'view',
                    $shop_id
                ]);
            }
        } else {
            $this->request->data = $this->Review->getData($shop_id, $user_id);
        }

        $isNew = empty($this->request->data);

        $this->set('shop_id', $shop_id);
        $this->set('isNew', $isNew);
    }

    public function delete($id = null) {
        if (!$this->Review->exists($id)) {
            throw new NotFoundException('レビューが見つかりません');
        }

        // ショップID を取得
        $shop_id = $this->Review->findById($id, ['Review.shop_id'])['Review']['shop_id'];

        $this->request->allowMethod('post', 'delete');
        if ($this->Review->delete($id)) {
            $this->Flash->success('レビューを削除しました。');

            return $this->redirect([
                'controller' => 'shops',
                'action' => 'view',
                $shop_id
            ]);
        } else {
            $this->Flash->error('レビューを削除できませんでした。');
        }
    }
}
