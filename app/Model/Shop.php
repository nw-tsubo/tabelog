<?php

class Shop extends AppModel {
    public $actsAs = [
        'Upload.Upload' => [
            'photo' => [
                'fields' => [
                    'dir' => 'photo_dir'
                ],
                'deleteOnUpdate' => true,
            ]
        ]
    ];

    public $hasMany = [
        'Review' => [
            'className' => 'Review',
            'dependent' => true,        // Shop が削除された時、Review も再帰的に削除するよう設定
        ]
    ];

    public $validate = [
        'name' => [
            'rule' => 'notBlank'
        ],
        'tel' => [
            'rule' => 'notBlank'
        ],
        'addr' => [
            'rule' => 'notBlank'
        ],
        'url' => [
            'rule' => ['url', true],
            'message' => '形式が正しくありません'
        ],
        'photo' => [
            'UnderPhpSizeLimit' => [
                'allowEmpty' => true,
                'rule' => 'isUnderPhpSizeLimit',
                'message' => 'アップロード可能なファイルサイズを超えています。'
            ],
            'BelowMaxSize' => [
                'rule' => ['isBelowMaxSize', 5242880],
                'message' => 'アップロード可能なファイルサイズを超えています。'
            ],
            'CompletedUpload' => [
                'rule' => 'isCompletedUpload',
                'message' => 'ファイルが正常にアップロードされませんでした。'
            ],
            'ValidMiimeType' => [
                'rule' => ['isValidMimeType', ['image/jpeg', 'image/png'], false],
                'message' => 'ファイルが JPEG でも PNG でもありません。'
            ],
            'ValidExtension' => [
                'rule' => ['isValidExtension', ['jpeg', 'jpg', 'png'], false],
                'message' => 'ファイルの拡張子が JPEG でも PNG でもありません。'
            ],
        ]
    ];
}
