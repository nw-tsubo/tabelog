<?php

class ShopsShell extends AppShell {
    public $uses = array('Shop');   // 使用するモデルを設定

    public function getOptionParser() {
        $parser = parent::getOptionParser();

        $parser->description(
            'レストランデータ管理プログラム'
        )->addSubcommand('create_dummies', array(
            'help' => 'ダミーのレストランデータ作成',
            'parser' => array(
                'description' => array(
                    'ダミーのレストランデータを作成します'
                ),
                'arguments' => array(
                    'number' => array(
                        'help' => '作成するデータの件数',
                        'required' => true,
                    ),
                ),
            )
        ))->addSubcommand('delete_all', array(
            'help' => 'レストランデータを全件削除',
            'parser' => array(
                'description' => array(
                    'レストランデータを全件削除します'
                ),
                'options' => array(
                    'force' => array(
                        'short' => 'f',
                        'help' => '確認メッセージなしで削除する',
                        'boolean' => true
                    )
                )
            )
        ));

        return $parser;
    }

    public function create_dummies() {
        $number = $this->args[0];

        for($i = 0; $i < $number; $i++) {
            $shop = array(
                'Shop' => array(
                    'name' => "ショップ{$i}",
                    'tel' => "999-$i-9999",
                    'addr' => "東京都新宿区 {$i}丁目",
                    'url' => "http://www$i.nowall.co.jp",
                ),
            );
            $this->Shop->create();
            if (! $this->Shop->save($shop)) {
                // 登録エラーならエラー内容を表示して処理を終了
                foreach($this->Shop->validationErrors as $error) {
                    $this->out($error);
                }
                break;
            }
        }

        $this->out("{$i} 件作成しました");
    }

    public function delete_all() {
        if (!$this->params['force']) {
            if (strtolower($this->in('本当に削除してもよろしいですか？', array('y', 'n'), 'n')) == 'n') {
                $this->out('終了します');
                return;
            }
        }

        /*
         *  全件削除する
         *
         *  条件を渡さないと削除してくれないので '1 = 1' で無理やりで全件対象とする。
         *  第２引数に true を渡してカスケード削除を行う（Shop が保有する Review も削除）
         */
        $this->Shop->deleteAll('1 = 1', true);

        $count = $this->Shop->getAffectedRows();
        $this->out("$count 件削除しました");
    }
}

