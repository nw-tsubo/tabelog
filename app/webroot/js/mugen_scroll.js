function addContents(data) {
    $('.content:last').after(
        $(data).find('#contents tbody').html()
    );
}

function replaceNavigation(data) {
    if ($(data).find('#scroll-nav a').length > 0){
        // 次ページへのリンクがある時
        // ナビゲーションを次ページへのリンクに置き換える
        $('#scroll-nav').html(
            $(data).find('#scroll-nav').html()
        );
    } else {
        // 最終ページの時
        // ナビゲーションを表示しない
        $('#scroll-nav').html('');
    }
}

var prosessing = false;

function mugenScroll() {
    if (prosessing) {
        return;
    }

    // 最終ページかチェック（次ページへのリンクがあるか？）
    if ($('#scroll-nav a').length === 0) {
        return;
    }
    //console.log($('#scroll-nav a').attr('href'));

    prosessing = true;

    // 非同期に次ページの内容を取得
    $.ajax({
        url: $('#scroll-nav a').attr('href')
    }).done(function (data) {
        //console.log(data);

        addContents(data);
        replaceNavigation(data);
    }).fail(function (data) {
        alert('Ajax 通信でエラーが発生しました');
    }).always(function () {
        prosessing = false;
    });
}

$(function() {
    $(window).on('scroll', function(e) {
        var bottomPosition = 100;
        var scrollHeight = $(document).height();
        var scrollPosition = $(window).height() + $(window).scrollTop();

        if (scrollPosition > (scrollHeight - bottomPosition)) {
            mugenScroll();
        }
    });
});
