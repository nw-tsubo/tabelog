<?php echo $this->Html->script('mugen_scroll', ['inline' => false]); ?>

<h2>レストラン一覧</h2>

<?php if ($current_user) : ?>
    <div style="text-align:right;">
        <span><?= $this->Html->link('新規追加', ['action' => 'add']); ?></span>
    </div>
<?php endif; ?>

<?= $this->element('Shops/list', ['shops' => $shops]); ?>

